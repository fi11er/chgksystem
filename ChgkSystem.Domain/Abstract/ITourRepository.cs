﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface ITourRepository: IGenericRepository<Tour>
	{
	}
}
