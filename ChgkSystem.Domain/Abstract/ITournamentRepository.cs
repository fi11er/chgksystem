﻿using System.Collections.Generic;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface ITournamentRepository: IGenericRepository<Tournament>
	{
		IEnumerable<Tournament> GetPast();
		IEnumerable<Tournament> GetActive();
		IEnumerable<Tournament> GetNext();
	}
}
