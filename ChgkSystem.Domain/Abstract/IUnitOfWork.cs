﻿namespace ChgkSystem.Domain.Abstract
{
	public interface IUnitOfWork
	{
		void Save();
		INewsRepository NewsRepository { get; }
		ITournamentRepository TournamentRepository { get; }
		IPlaygroundRepository PlaygroundRepository { get; }
		ITeamRepository TeamRepository { get; }
		ITourRepository TourRepository { get; }
		IAppealRepository AppealRepository { get; }
		IAnswerRepository AnswerRepository { get; }
	}
}
