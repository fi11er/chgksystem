﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface IPlaygroundRepository: IGenericRepository<Playground>
	{
	}
}
