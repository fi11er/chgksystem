﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ChgkSystem.Domain.Abstract
{

	public abstract class EFGenericRepository<T> : IGenericRepository<T> where T: class
	{
		protected DbContext context;
		protected readonly IDbSet<T> dbSet;

		public EFGenericRepository(DbContext context)
		{
			this.context = context;
			dbSet = this.context.Set<T>();
		}

		public virtual IEnumerable<T> GetAll()
		{

			return dbSet.AsEnumerable<T>();
		}

		public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
		{

			IEnumerable<T> query = dbSet.Where(predicate).AsEnumerable();
			return query;
		}

		public T FindOneBy(Expression<Func<T, bool>> predicate)
		{
			return dbSet.FirstOrDefault(predicate);
		}

		public virtual T Add(T entity)
		{
			var result = dbSet.Add(entity);
			Save();
			return result;
		}

		public virtual T Delete(T entity)
		{
			var result = dbSet.Remove(entity);
			Save();
			return result;
		}

		public virtual void Edit(T entity)
		{
			context.Entry(entity).State = EntityState.Modified;
			Save();
		}

		public virtual void Save()
		{
			context.SaveChanges();
		}
	}
}
