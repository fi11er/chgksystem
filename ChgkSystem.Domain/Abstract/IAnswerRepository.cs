﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface IAnswerRepository : IGenericRepository<Answer>
	{
	}
}
