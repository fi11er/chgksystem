﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface INewsRepository: IGenericRepository<News>
	{
	}
}
