﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Abstract
{
	public interface ITeamRepository: IGenericRepository<Team>
	{
	}
}
