using System.Collections.Generic;
using ChgkSystem.Domain.Concrete;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChgkSystem.Domain.Migrations
{
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<ChgkSystem.Domain.Concrete.EFDbContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(ChgkSystem.Domain.Concrete.EFDbContext context)
		{
			EFUserManager userManager = new EFUserManager(new UserStore<User>(context));
			EFRoleManager roleManager = new EFRoleManager(new RoleStore<Role>(context));

			string[] roles =
		    {
			    "Administrators",
			    "Managers"
		    };
			string userName = "test@test.test";
			string password = "password";
			string userRole = roles[0];

			foreach (var role in roles)
			{
				if (!roleManager.RoleExists(role))
				{
					roleManager.Create(new Role(role));
				}
			}

			User user = userManager.FindByName(userName);
			if (user == null)
			{
				userManager.Create(new User
				{
					UserName = userName,
					Email = userName
				}, password);
				user = userManager.FindByName(userName);
			}
			if (!userManager.IsInRole(user.Id, userRole))
			{
				userManager.AddToRole(user.Id, userRole);
			}

			context.SaveChanges();
		}
	}
}
