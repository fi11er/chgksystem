namespace ChgkSystem.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appeals",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AppealType = c.Int(nullable: false),
                        AppealStatus = c.Int(nullable: false),
                        Text = c.String(),
                        TeamID = c.Int(nullable: false),
                        QuestionID = c.Int(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.QuestionID)
                .ForeignKey("dbo.Teams", t => t.TeamID)
                .Index(t => t.TeamID)
                .Index(t => t.QuestionID);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        TourID = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        IsIncorrect = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tours", t => t.TourID)
                .Index(t => t.TourID);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        QuestionID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.QuestionID)
                .Index(t => t.QuestionID);
            
            CreateTable(
                "dbo.AnswerDecisions",
                c => new
                    {
                        UserID = c.String(nullable: false, maxLength: 128),
                        AnswerID = c.Int(nullable: false),
                        Decision = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.AnswerID })
                .ForeignKey("dbo.Answers", t => t.AnswerID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.AnswerID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Tournaments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        SinceDate = c.DateTime(nullable: false),
                        UntilDate = c.DateTime(nullable: false),
                        RequestsUntilDate = c.DateTime(),
                        UserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        TournamentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tournaments", t => t.TournamentID)
                .Index(t => t.TournamentID);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Status = c.Int(nullable: false),
                        PlaygroundID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Playgrounds", t => t.PlaygroundID)
                .Index(t => t.PlaygroundID);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TeamID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Teams", t => t.TeamID)
                .Index(t => t.TeamID);
            
            CreateTable(
                "dbo.Playgrounds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        TournamentID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        UserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tournaments", t => t.TournamentID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.TournamentID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        PlaygroundID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Playgrounds", t => t.PlaygroundID)
                .Index(t => t.PlaygroundID);
            
            CreateTable(
                "dbo.TeamAnswers",
                c => new
                    {
                        TeamID = c.Int(nullable: false),
                        AnswerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TeamID, t.AnswerID })
                .ForeignKey("dbo.Answers", t => t.AnswerID)
                .ForeignKey("dbo.Teams", t => t.TeamID)
                .Index(t => t.TeamID)
                .Index(t => t.AnswerID);
            
            CreateTable(
                "dbo.Tours",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ReportsUntilDate = c.DateTime(nullable: false),
                        ApealsUntilDate = c.DateTime(nullable: false),
                        TournamentID = c.Int(nullable: false),
                        QuestionsVisibility = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tournaments", t => t.TournamentID)
                .Index(t => t.TournamentID);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        PlaygroundID = c.Int(nullable: false),
                        TourID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.PlaygroundID, t.TourID })
                .ForeignKey("dbo.Playgrounds", t => t.PlaygroundID)
                .ForeignKey("dbo.Tours", t => t.TourID)
                .Index(t => t.PlaygroundID)
                .Index(t => t.TourID);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Text = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.TeamGroups",
                c => new
                    {
                        Team_ID = c.Int(nullable: false),
                        Group_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Team_ID, t.Group_ID })
                .ForeignKey("dbo.Teams", t => t.Team_ID, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.Group_ID, cascadeDelete: true)
                .Index(t => t.Team_ID)
                .Index(t => t.Group_ID);
            
            CreateTable(
                "dbo.UserTournaments",
                c => new
                    {
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Tournament_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Tournament_ID })
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tournaments", t => t.Tournament_ID, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Tournament_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Appeals", "TeamID", "dbo.Teams");
            DropForeignKey("dbo.Appeals", "QuestionID", "dbo.Questions");
            DropForeignKey("dbo.Answers", "QuestionID", "dbo.Questions");
            DropForeignKey("dbo.AnswerDecisions", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserTournaments", "Tournament_ID", "dbo.Tournaments");
            DropForeignKey("dbo.UserTournaments", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tournaments", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tours", "TournamentID", "dbo.Tournaments");
            DropForeignKey("dbo.Reports", "TourID", "dbo.Tours");
            DropForeignKey("dbo.Reports", "PlaygroundID", "dbo.Playgrounds");
            DropForeignKey("dbo.Questions", "TourID", "dbo.Tours");
            DropForeignKey("dbo.Groups", "TournamentID", "dbo.Tournaments");
            DropForeignKey("dbo.TeamAnswers", "TeamID", "dbo.Teams");
            DropForeignKey("dbo.TeamAnswers", "AnswerID", "dbo.Answers");
            DropForeignKey("dbo.Playgrounds", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Playgrounds", "TournamentID", "dbo.Tournaments");
            DropForeignKey("dbo.Teams", "PlaygroundID", "dbo.Playgrounds");
            DropForeignKey("dbo.Locations", "PlaygroundID", "dbo.Playgrounds");
            DropForeignKey("dbo.Players", "TeamID", "dbo.Teams");
            DropForeignKey("dbo.TeamGroups", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.TeamGroups", "Team_ID", "dbo.Teams");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AnswerDecisions", "AnswerID", "dbo.Answers");
            DropIndex("dbo.UserTournaments", new[] { "Tournament_ID" });
            DropIndex("dbo.UserTournaments", new[] { "User_Id" });
            DropIndex("dbo.TeamGroups", new[] { "Group_ID" });
            DropIndex("dbo.TeamGroups", new[] { "Team_ID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Reports", new[] { "TourID" });
            DropIndex("dbo.Reports", new[] { "PlaygroundID" });
            DropIndex("dbo.Tours", new[] { "TournamentID" });
            DropIndex("dbo.TeamAnswers", new[] { "AnswerID" });
            DropIndex("dbo.TeamAnswers", new[] { "TeamID" });
            DropIndex("dbo.Locations", new[] { "PlaygroundID" });
            DropIndex("dbo.Playgrounds", new[] { "UserID" });
            DropIndex("dbo.Playgrounds", new[] { "TournamentID" });
            DropIndex("dbo.Players", new[] { "TeamID" });
            DropIndex("dbo.Teams", new[] { "PlaygroundID" });
            DropIndex("dbo.Groups", new[] { "TournamentID" });
            DropIndex("dbo.Tournaments", new[] { "UserID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AnswerDecisions", new[] { "AnswerID" });
            DropIndex("dbo.AnswerDecisions", new[] { "UserID" });
            DropIndex("dbo.Answers", new[] { "QuestionID" });
            DropIndex("dbo.Questions", new[] { "TourID" });
            DropIndex("dbo.Appeals", new[] { "QuestionID" });
            DropIndex("dbo.Appeals", new[] { "TeamID" });
            DropTable("dbo.UserTournaments");
            DropTable("dbo.TeamGroups");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.News");
            DropTable("dbo.Reports");
            DropTable("dbo.Tours");
            DropTable("dbo.TeamAnswers");
            DropTable("dbo.Locations");
            DropTable("dbo.Playgrounds");
            DropTable("dbo.Players");
            DropTable("dbo.Teams");
            DropTable("dbo.Groups");
            DropTable("dbo.Tournaments");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AnswerDecisions");
            DropTable("dbo.Answers");
            DropTable("dbo.Questions");
            DropTable("dbo.Appeals");
        }
    }
}
