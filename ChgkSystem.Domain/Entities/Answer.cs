﻿using System.Collections.Generic;

namespace ChgkSystem.Domain.Entities
{
	public enum AnswerStatus
	{
		Pending,
		Correct,
		Incorrect
	}
	public class Answer
	{
		public int ID { get; set; }
		public string Text { get; set; }
		public int QuestionID { get; set; }
		public AnswerStatus Status { get; set; }

		public virtual Question Question { get; set; }
		public virtual ICollection<TeamAnswer> TeamAnswers { get; set; }
		public virtual ICollection<AnswerDecision> Decisions { get; set; } 
	}
}
