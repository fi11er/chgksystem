﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ChgkSystem.Domain.Entities
{
	public class Tournament
	{
		public int ID { get; set; }
		[Display(Name = "Название *")]
		[Required(ErrorMessage = "Введите название")]
		public string Title { get; set; }
		[Display(Name = "Описание")]
		public string Description { get; set; }
		[Required(ErrorMessage = "Укажите дату начала")]
		public DateTime? SinceDate { get; set; }
		[Required(ErrorMessage = "Укажите дату окончания")]
		public DateTime? UntilDate { get; set; }
		public DateTime? RequestsUntilDate { get; set; }
		public string UserID { get; set; }

		public virtual ICollection<Playground> Playgrounds { get; set; }
		public virtual ICollection<Tour> Tours { get; set; }
		public virtual ICollection<Group> Groups { get; set; }
		public virtual ICollection<User> Jury { get; set; } 
		public virtual User User { get; set; }

		[NotMapped]
		public virtual IEnumerable<Playground> AcceptedPlaygrounds
		{
			get
			{
				return Playgrounds.Where(p => p.Status == PlaygroundStatus.Accepted);
			}
		}

		[NotMapped]
		public virtual IEnumerable<Playground> PendingPlaygrounds
		{
			get
			{
				return Playgrounds.Where(p => p.Status == PlaygroundStatus.Pending);
			}
		}

		[NotMapped]
		public IEnumerable<Team> Teams
		{
			get
			{
				return AcceptedPlaygrounds.SelectMany(m => m.Teams);
			}
		}
	}
}
