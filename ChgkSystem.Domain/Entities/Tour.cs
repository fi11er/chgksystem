﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ChgkSystem.Domain.Entities
{
	public enum QuestionsVisibility
	{
		ForMe,
		ForPlaygroundManager,
		ForAll
	}

	public enum TourStatus
	{
		WaitingReports,
		CheckingProtocols,
		WaitingAppeals,
		Finished
	}

	public class Tour
	{
		public int ID { get; set; }
		public int Number { get; set; }

		[Display(Name = "Дата проведения *")]
		[Required(ErrorMessage = "Укажите дату проведения")]
		public DateTime? Date { get; set; }

		[Display(Name = "Срок подачи отчетов *")]
		[Required(ErrorMessage = "Укажите срок подачи отчетов")]
		public DateTime? ReportsUntilDate { get; set; }

		[Display(Name = "Срок подачи апелляций *")]
		[Required(ErrorMessage = "Укажите срок подачи апелляций")]
		public DateTime? ApealsUntilDate { get; set; }

		//public int? LinkedTourID { get; set; }
		public int TournamentID { get; set; }
		public QuestionsVisibility QuestionsVisibility { get; set; }
		public TourStatus Status { get; set; }

		//public virtual Tour LinkedTour { get; set; }
		public virtual Tournament Tournament { get; set; }
		public virtual ICollection<Question> Questions { get; set; }
		public virtual ICollection<Report> Reports { get; set; }
		
		[NotMapped]
		public virtual IEnumerable<Appeal> Appeals
		{
			get
			{
				return Questions.SelectMany(m => m.Appeals);
			}
		}

	}
}
