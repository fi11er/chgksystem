﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChgkSystem.Domain.Entities
{
	public class News
	{
		public int ID { get; set; }
		[Required(ErrorMessage = "Введите заголовок новости")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Введите текст новости")]
		public string Text { get; set; }
		public DateTime Date { get; set; }
	}
}
