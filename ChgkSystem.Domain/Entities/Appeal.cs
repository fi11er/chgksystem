﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChgkSystem.Domain.Entities
{
	public enum AppealType
	{
		AnswerAccept,
		QuestionDecline
	}

	public enum AppealStatus
	{
		Pending,
		Accepted,
		Rejected
	}

	public class Appeal
	{
		public int ID { get; set; }
		public AppealType AppealType { get; set; }
		public AppealStatus AppealStatus { get; set; }
		public string Text { get; set; }
		public int TeamID { get; set; }
		public int QuestionID { get; set; }
		public string Email { get; set; }

		public virtual Team Team { get; set; }
		public virtual Question Question { get; set; }

		[NotMapped]
		public virtual Answer Answer
		{
			get
			{
				return Question.Answers.FirstOrDefault(a => a.TeamAnswers.Any(ta => ta.TeamID == TeamID));
			}
		}
	}
}
