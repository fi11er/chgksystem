﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ChgkSystem.Domain.Entities
{
	public class Question
	{
		public int ID { get; set; }
		public string Text { get; set; }
		public int TourID { get; set; }
		public int Number { get; set; }
		public bool IsIncorrect { get; set; }

		public virtual Tour Tour { get; set; }
		public virtual ICollection<Answer> Answers { get; set; }
		public virtual ICollection<Appeal> Appeals { get; set; }
			
		[NotMapped]
		public virtual IEnumerable<Answer> CorrectAnswers
		{
			get
			{
				return Answers.Where(m => m.Status == AnswerStatus.Correct);
			}
		}

		[NotMapped]
		public virtual IEnumerable<Answer> IncorrectAnswers
		{
			get
			{
				return Answers.Where(m => m.Status == AnswerStatus.Incorrect);
			}
		}

		[NotMapped]
		public virtual IEnumerable<Answer> PendingAnswers
		{
			get
			{
				return Answers.Where(m => m.Status == AnswerStatus.Pending);
			}
		}
	}
}
