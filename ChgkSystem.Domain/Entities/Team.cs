﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChgkSystem.Domain.Entities
{
	public enum TeamStatus
	{
		Pending,
		Accepted,
		Rejected
	}

	public class Team
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public TeamStatus Status { get; set; }
		public int PlaygroundID { get; set; }

		[NotMapped]
		public string Code
		{
			get { return "T" + ID; }
		}

		public virtual Playground Playground { get; set; }
		public virtual ICollection<Player> Players { get; set; }
		public virtual ICollection<Group> Groups { get; set; }
		public virtual ICollection<TeamAnswer> TeamAnswers { get; set; }
	}
}
