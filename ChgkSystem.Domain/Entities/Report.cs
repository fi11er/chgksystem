﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChgkSystem.Domain.Entities
{
	public class Report
	{
		[Key, ForeignKey("Playground"), Column(Order = 0)]
		public int PlaygroundID { get; set; }
		[Key, ForeignKey("Tour"), Column(Order = 1)]
		public int TourID { get; set; }

		public DateTime Date { get; set; }

		public virtual Playground Playground { get; set; }
		public virtual Tour Tour { get; set; }
	}
}
