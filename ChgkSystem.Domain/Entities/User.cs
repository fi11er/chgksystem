﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChgkSystem.Domain.Entities
{
	public class User: IdentityUser
	{
		[InverseProperty("Jury")]
		public virtual ICollection<Tournament> TournamentsJury { get; set; } 
	}
}
