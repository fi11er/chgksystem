﻿using System.Collections.Generic;

namespace ChgkSystem.Domain.Entities
{
	public enum PlaygroundStatus
	{
		Pending,
		Accepted,
		Rejected
	}

	public class Playground
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public int TournamentID { get; set; }
		public PlaygroundStatus Status { get; set; }
		public string UserID { get; set; }


		public virtual Tournament Tournament { get; set; }
		public virtual ICollection<Location> Locations { get; set; }
		public virtual ICollection<Team> Teams { get; set; }
		public virtual User User { get; set; }
	}
}
