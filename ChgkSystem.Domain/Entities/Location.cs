﻿namespace ChgkSystem.Domain.Entities
{
	public class Location
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public int PlaygroundID { get; set; }

		public virtual Playground Plaground { get; set; }
	}
}
