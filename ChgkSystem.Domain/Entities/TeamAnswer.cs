﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChgkSystem.Domain.Entities
{
	public class TeamAnswer
	{
		[Key, ForeignKey("Team"), Column(Order = 0)]
		public int TeamID { get; set; }
		[Key, ForeignKey("Answer"), Column(Order = 1)]
		public int AnswerID { get; set; }

		public virtual Team Team { get; set; }
		public virtual Answer Answer { get; set; }
	}
}
