﻿namespace ChgkSystem.Domain.Entities
{
	public class Player
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public int TeamID { get; set; }

		public virtual Team Team { get; set; }
	}
}
