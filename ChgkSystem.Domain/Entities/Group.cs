﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ChgkSystem.Domain.Entities
{
	public class Group
	{
		public int ID { get; set; }
		[DisplayName("Название группы *")]
		[Required(ErrorMessage = "Введите название группы")]
		public string Title { get; set; }
		[DisplayName("Описание")]
		public string Description { get; set; }
		public int TournamentID { get; set; }

		public virtual Tournament Tournament { get; set; }
		public virtual ICollection<Team> Teams { get; set; }
	}
}
