﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChgkSystem.Domain.Entities
{
	public enum JuryDecision
	{
		Correct,
		Incorrect
	}

	public class AnswerDecision
	{
		[Key, ForeignKey("User"), Column(Order = 0)]
		public string UserID { get; set; }
		[Key, ForeignKey("Answer"), Column(Order = 1)]
		public int AnswerID { get; set; }
		public JuryDecision Decision { get; set; }

		public virtual User User { get; set; }
		public virtual Answer Answer { get; set; }


	}
}
