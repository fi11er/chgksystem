﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChgkSystem.Domain.Concrete
{
	public class EFDbContext: IdentityDbContext<User>
	{
		public DbSet<News> News { get; set; }
		public DbSet<Tournament> Tournaments { get; set; }
		public DbSet<Group> Groups { get; set; }
		public DbSet<Location> Locations { get; set; }
		public DbSet<Playground> Playgrounds { get; set; }
		public DbSet<Tour> Tours { get; set; }
		public DbSet<Team> Teams { get; set; }
		public DbSet<Player> Players { get; set; }
		public DbSet<Appeal> Appeals { get; set; }

		public EFDbContext(): base("ChgkDb")
		{
		}

		static EFDbContext()
		{
			Database.SetInitializer<EFDbContext>(new EFDbInit());
		}

		private static EFDbContext context = new EFDbContext();

		public static EFDbContext Create()
		{
			return new EFDbContext();
		}
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			Database.SetInitializer<EFDbContext>(new EFDbInit());
			base.OnModelCreating(modelBuilder);
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
		}
	}
}
