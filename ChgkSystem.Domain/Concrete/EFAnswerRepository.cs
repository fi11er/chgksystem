﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	class EFAnswerRepository: EFGenericRepository<Answer>, IAnswerRepository
	{
		public EFAnswerRepository(EFDbContext context)
			: base(context)
		{
		}
	}
}
