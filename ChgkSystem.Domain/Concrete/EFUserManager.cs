﻿using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace ChgkSystem.Domain.Concrete
{
	public class EFUserManager: UserManager<User>
	{
		public EFUserManager(IUserStore<User> store)
			: base(store)
		{
		}

		public static EFUserManager Create(
			IdentityFactoryOptions<EFUserManager> options,
			IOwinContext context)
		{
			EFDbContext db = context.Get<EFDbContext>();
			EFUserManager manger = new EFUserManager(new UserStore<User>(db));
			return manger;
		}

	}
}
