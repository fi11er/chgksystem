﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFAppealRepository : EFGenericRepository<Appeal>, IAppealRepository
	{
		public EFAppealRepository(EFDbContext context)
			: base(context)
		{
		}
	}
}
