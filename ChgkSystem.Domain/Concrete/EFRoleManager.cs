﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace ChgkSystem.Domain.Concrete
{
	public class EFRoleManager : RoleManager<Role>, IDisposable
	{
		public EFRoleManager(RoleStore<Role> store) : base(store)
		{
		}

		public static EFRoleManager Create(IdentityFactoryOptions<EFRoleManager> options, IOwinContext context)
		{
			return new EFRoleManager(new RoleStore<Role>(context.Get<EFDbContext>()));
		}
	}
}
