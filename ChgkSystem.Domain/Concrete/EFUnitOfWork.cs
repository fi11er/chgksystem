﻿using ChgkSystem.Domain.Abstract;

namespace ChgkSystem.Domain.Concrete
{
	public class EFUnitOfWork: IUnitOfWork
	{
		protected EFDbContext context = new EFDbContext();
		private INewsRepository newsRepository;
		private ITournamentRepository tournamentRepository;
		private IPlaygroundRepository playgroundRepository;
		private ITeamRepository teamRepoitory;
		private ITourRepository tourRepository;
		private IAppealRepository appealRepository;
		private IAnswerRepository answerRepository;

		public INewsRepository NewsRepository
		{
			get { return newsRepository ?? (newsRepository = new EFNewsRepository(context)); }
		}

		public ITournamentRepository TournamentRepository
		{
			get { return tournamentRepository ?? (tournamentRepository = new EFTournamentRepository(context)); }
		}

		public IPlaygroundRepository PlaygroundRepository
		{
			get { return playgroundRepository ?? (playgroundRepository = new EFPlaygroundRepository(context)); }
		}

		public ITeamRepository TeamRepository
		{
			get { return teamRepoitory ?? (teamRepoitory = new EFTeamRepository(context)); }
		}

		public ITourRepository TourRepository
		{
			get { return tourRepository ?? (tourRepository = new EFTourRepository(context)); }
		}

		public IAppealRepository AppealRepository
		{
			get { return appealRepository ?? (appealRepository = new EFAppealRepository(context)); }
		}

		public IAnswerRepository AnswerRepository
		{
			get { return answerRepository ?? (answerRepository = new EFAnswerRepository(context)); }
		}

		public void Save()
		{
			context.SaveChanges();
		}
	}
}
