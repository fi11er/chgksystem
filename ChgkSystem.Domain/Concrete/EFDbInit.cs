﻿using System.Data.Entity;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChgkSystem.Domain.Concrete
{
	public class EFDbInit : DropCreateDatabaseIfModelChanges<EFDbContext>
	{
		protected override void Seed(EFDbContext context)
		{
			base.Seed(context);
			PerformInitialSetup(context);
		}

		public void PerformInitialSetup(EFDbContext context)
		{
			EFUserManager userManager = new EFUserManager(new UserStore<User>(context));
			EFRoleManager roleManager = new EFRoleManager(new RoleStore<Role>(context));
			System.Console.WriteLine("init setup");

			string[] roles = {
				"Administrators",
				"Managers"
			};
			string email = "test@test.test";
			string password = "password";

			foreach (var role in roles)
			{
				if (!roleManager.RoleExists(role))
				{
					roleManager.Create(new Role(role));
				}
			}
			

			User user = userManager.FindByName(email);
			if (user == null)
			{
				userManager.Create(new User {UserName = email, Email = email}, password);
				user = userManager.FindByName(email);
			}
			if (!userManager.IsInRole(user.Id, "Administrators"))
			{
				userManager.AddToRole(user.Id, "Administrators");
			}
		}
	}
}
