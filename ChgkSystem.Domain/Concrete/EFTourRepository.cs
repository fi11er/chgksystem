﻿using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFTourRepository: EFGenericRepository<Tour>, ITourRepository
	{
		public EFTourRepository(EFDbContext context): base(context)
		{
		}
	}
}
