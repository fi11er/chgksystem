﻿using System;
using System.Collections.Generic;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFTournamentRepository: EFGenericRepository<Tournament>, ITournamentRepository
	{
		public EFTournamentRepository(EFDbContext context): base(context)
		{
		}

		public IEnumerable<Tournament> GetPast()
		{
			return FindBy(m => m.UntilDate < DateTime.Today);
		}

		public IEnumerable<Tournament> GetActive()
		{
			return FindBy(m => m.SinceDate < DateTime.Today && DateTime.Today < m.UntilDate);
		}

		public IEnumerable<Tournament> GetNext()
		{
			return FindBy(m => DateTime.Today < m.SinceDate);
		}
	}
}
