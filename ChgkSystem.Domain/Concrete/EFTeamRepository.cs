﻿using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFTeamRepository: EFGenericRepository<Team>, ITeamRepository
	{
		public EFTeamRepository(EFDbContext context): base(context)
		{
		}
	}
}
