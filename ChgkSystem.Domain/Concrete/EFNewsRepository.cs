﻿using System;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFNewsRepository: EFGenericRepository<News>, INewsRepository
	{
		public EFNewsRepository(EFDbContext context): base(context)
		{
		}

		public override News Add(News news)
		{
			news.Date = DateTime.Now;
			return base.Add(news);
		}
	}
}
