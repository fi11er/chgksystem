﻿using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Domain.Concrete
{
	public class EFPlaygroundRepository: EFGenericRepository<Playground>, IPlaygroundRepository
	{
		public EFPlaygroundRepository(EFDbContext context): base(context)
		{
		}
	}
}
