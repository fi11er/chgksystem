﻿using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ChgkSystem.Domain.Concrete;

namespace ChgkSystem.WebUI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
