﻿function checkHeaders(response) {
	var jsonHeader = response.getResponseHeader('X-Responded-JSON');
	if (jsonHeader) {
		var data = JSON.parse(jsonHeader);
		if (data.status == 401) {
			window.location.href = data.headers.location;
			return false;
		}
	}
	return true;
}

function onSuccessGeneric(data, status, response) {
	if (!checkHeaders(response)) {
		return;
	}
	if (data.status == 'OK') {
		if (data.URL) {
			window.location.assign(data.url);
		}
		window.location.reload();
	} else {
		$(this).replaceWith(data);
	}
}

function onBeginLogin() {
}

function onSuccessLogin(data) {
	if (data.length == 0) {
		window.location.reload();
	} else {
		$('#addGroupModal .modal-form-container').html(data);
	}
}

function onBeginRegister() {
}

function onSuccessRegister(data) {
	if (data.length == 0) {
		window.location.reload();
	} else {
		$('#addGroupModal .modal-form-container').html(data);
	}
}

function onBeginSubmitPlayground() {
}

function onSuccessSubmitPlayground(data) {
	if (data.length == 0) {
		window.location.href = '/';
	} else {
		$('#addGroupModal .modal-form-container').html(data);
	}
}

function openQuestionsModal() {
	$('#addQuestionsModal').find('form input#TourID').val($(this).data('tour'));
}

function openAddReportModal() {
	$('#addReportModal').find('form input#TourID').val($(this).data('tour'));
}


$('form#addQuestionsFileForm').ajaxForm({
	success: onSuccessGeneric
});

$('form#addReportFileForm').ajaxForm({
	success: onSuccessGeneric
});

function updateAuthentication() {
	$('[data-authentication]').each(function () {
		var elem = $(this);
		$.get(elem.data('authentication'), function (data) {
			elem.replaceWith(data);
		});
	});
}

$('#inputDates').datepicker({
	format: "dd.mm.yyyy",
	todayBtn: "linked",
	language: "ru",
	multidate: false,
	autoclose: true,
	todayHighlight: true
});

$('[data-toggle=modal][data-url]').on('click', function () {
	var url = $(this).data('url');
	var target = $(this).data('target') + ' .modal-container';
	$(target).html('');
	var callback = window[$(this).data('callback')];
	if (typeof (callback) == 'function') {
		$(target).load(url, callback);
	} else {
		$(target).load(url)
	}
});

$('[data-script]').each(function () {
	var script = window[$(this).data('script')];
	if (typeof (script) == 'function') {
		$(this).on('click', script);
	}
});

$('a[data-post]').on('click', function () {
	var callback = window[$(this).data('callback')];
	$.post($(this).data('post'), function () {
		if (typeof (callback) == 'function') {
			callback();
		}
	});
});

function addUnobtrusiveValidation() {
	$.validator.unobtrusive.parse($(this));
}

$(function () {
	$.validator.addMethod('required_group', function (value, element) {
		var $module = $(element).parents('form');
		return $module.find('input:checkbox:checked').length;
	}, 'Укажите хотя бы одну группу');
	$.validator.addClassRules('required-checkbox', { 'required_group': true });
});

$(function () {
	$.validator.addMethod(
    "date",
    function (value, element) {
    	var bits = value.split("."), str;
    	if (!bits)
    		return this.optional(element) || false;
    	str = bits[1] + '/' + bits[0] + '/' + bits[2];
    	return this.optional(element) || !/Invalid|NaN/.test(new Date(str));
    },
    ""
    );
});




$.validator.unobtrusive.parse();


$('#inputDate').datepicker({
	format: "dd.mm.yyyy",
	todayBtn: "linked",
	language: "ru",
	autoclose: true,
	todayHighlight: true
});
$('#inputReportsUntil').datepicker({
	format: "dd.mm.yyyy",
	todayBtn: "linked",
	language: "ru",
	autoclose: true,
	todayHighlight: true
});
$('#inputApealUntil').datepicker({
	format: "dd.mm.yyyy",
	todayBtn: "linked",
	language: "ru",
	autoclose: true,
	todayHighlight: true
});
$('#addTourModal').scroll(function () {
	$('#inputDate').datepicker('place')
	$('#inputReportsUntil').datepicker('place')
	$('#inputApealUntil').datepicker('place')
});