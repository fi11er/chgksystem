﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Models;

namespace ChgkSystem.WebUI.Controllers
{
    public class ResultsController : Controller
    {
		private IUnitOfWork unitOfWork;
		public ResultsController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

	    public ActionResult All(int tournamentID, int playgroundID = 0, int groupID = 0, int tourID = 0)
	    {
		    var tournament = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == tournamentID);
		    if (groupID == 0)
		    {
			    groupID = tournament.Groups.First().ID;
		    }
		    var teams = tournament.Teams.Where(t => t.Groups.Any(g => g.ID == groupID));
		    if (playgroundID > 0)
		    {
			    teams = teams.Where(m => m.PlaygroundID == playgroundID);
		    }
		    var teamsViewModel = teams.Select(team => new TeamViewModel
		    {
				Code = team.Code,
				Name = team.Name,
				Playground = team.Playground.Title,
				Tours = tournament.Tours.Select(
					tour => team.TeamAnswers.Count(
						m => m.Answer.Question.TourID == tour.ID 
							&& m.Answer.Status == AnswerStatus.Correct 
							&& !m.Answer.Question.IsIncorrect)),
				Total = team.TeamAnswers.Count(m => m.Answer.Status == AnswerStatus.Correct && !m.Answer.Question.IsIncorrect)
		    }).OrderBy(m => m.Total);

		    var result = new ResultsViewModel
		    {
				TourID = tourID,
				GroupID = groupID,
				PlaygroundID = playgroundID,
			    Tournament = tournament,
			    Teams = teamsViewModel
		    };

		    return PartialView("Results/_All", result);
	    }
    }
}