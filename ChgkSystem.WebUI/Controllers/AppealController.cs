﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Models;

namespace ChgkSystem.WebUI.Controllers
{
    public class AppealController : Controller
    {
		private IUnitOfWork unitOfWork;
		public AppealController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

	    [HttpGet]
	    public ActionResult Add(int tourID)
	    {
		    Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == tourID);
		    AddAppealFormModel model = new AddAppealFormModel
		    {
				Tour = tour,
				TourID = tourID,
				Appeal = new Appeal()
		    };
			return PartialView("Appeal/_AddForm", model);
	    }

		[HttpPost]
		public ActionResult Add(AddAppealFormModel form)
		{
			if (ModelState.IsValid)
			{
				unitOfWork.AppealRepository.Add(form.Appeal);
				//string url = Url.Action("details", "tournament", new { ID = form. }) + "#tours";
				return Json(new { status = "OK" });
			}
			form.Tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
			return PartialView("Appeal/_AddForm", form);
		}

	    public ActionResult List(int tourID)
		{
			Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == tourID);
		    AppealsViewModel model = new AppealsViewModel
		    {
				TourID = tourID,
			    Appeals = tour.Appeals.Where(m => m.AppealStatus == AppealStatus.Pending),
			    AppealsText = "asdasd"
		    };
			return PartialView("Appeal/_List", model);

	    }

	    [HttpPost]
	    public ActionResult Review(AppealReviewFormModel form)
	    {
		    foreach (var review in form.Reviews)
		    {
				int appealId = int.Parse(review.Key);
				var appeal = unitOfWork.AppealRepository.FindOneBy(m => m.ID == appealId);
			    if (review.Value == AppealStatus.Accepted)
			    {
				    if (appeal.AppealType == AppealType.AnswerAccept)
				    {
					    appeal.Answer.Status = AnswerStatus.Correct;
				    } 
					else if (appeal.AppealType == AppealType.QuestionDecline)
					{
						appeal.Question.IsIncorrect = true;
					}
			    }
			    appeal.AppealStatus = review.Value;
				unitOfWork.AppealRepository.Edit(appeal);
		    }
		    Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
		    if (tour.Appeals.All(m => m.AppealStatus != AppealStatus.Pending) && tour.ApealsUntilDate < DateTime.Now)
		    {
			    tour.Status = TourStatus.Finished;
		    }
			return Json(new { status = "OK" });
	    }
    }
}