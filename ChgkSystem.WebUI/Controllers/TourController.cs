﻿using System.Linq;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class TourController : Controller
    {
		private IUnitOfWork unitOfWork;
		public TourController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult Add(Tour tour)
		{
			if (ModelState.IsValid)
			{
				Tournament t = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == tour.TournamentID);
				tour.Number = t.Tours.Count() + 1;
				if (t.UserID != HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}

				t.Tours.Add(tour);
				unitOfWork.TournamentRepository.Edit(t);
				string url = Url.Action("details", "tournament", new { ID = t.ID }) + "#tours";
				return Json(new { status = "OK", url = url });
			}
			else
			{
				return PartialView("Tour/_AddForm", tour);
			}
		}

		public ActionResult Questions(int tourID)
		{
			Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == tourID);
			return PartialView("Tour/_Questions", tour);
		}
    }
}