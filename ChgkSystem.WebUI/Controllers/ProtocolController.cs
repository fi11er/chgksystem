﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class ProtocolController : Controller
    {
		private IUnitOfWork unitOfWork;
		public ProtocolController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		public ActionResult Modal(int tourID)
		{
			Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == tourID);
			return PartialView("Protocol/_Modal", tour);
		}

		[HttpPost]
	    public ActionResult Check(CheckProtocolFormModel form)
	    {
			if (ModelState.IsValid)
			{
				var userId = User.Identity.GetUserId();
				foreach (var decision in form.Answers)
				{
					var answerId = Convert.ToInt32(decision.Key);
					var answer =
						unitOfWork.AnswerRepository.FindOneBy(
							a => a.ID == answerId && a.Decisions.All(d => d.UserID != userId));
					if (answer == null)
					{
						return Json(new {status = "Error"});
					}
					answer.Decisions.Add(new AnswerDecision
					{
						Decision = decision.Value,
						UserID = userId
					});
					if (answer.Decisions.Count(a => a.Decision == decision.Value) == answer.Question.Tour.Tournament.Jury.Count
					    && answer.Status == AnswerStatus.Pending)
					{
						answer.Status = (decision.Value == JuryDecision.Correct) ? AnswerStatus.Correct : AnswerStatus.Incorrect;
					}
				}
				unitOfWork.Save();
				if (!unitOfWork.AnswerRepository.FindBy(m => m.Question.TourID == form.TourID && m.Status == AnswerStatus.Pending).Any())
				{
					Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
					tour.Status = TourStatus.WaitingAppeals;
					unitOfWork.TourRepository.Edit(tour);
				}
				return Json(new { status = "OK" });
			}
			else
			{
				Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
				return PartialView("Protocol/_Modal", tour);
			}
	    }

	    public ActionResult Discuss(DiscussProtocolFormModel form)
	    {
		    throw new NotImplementedException();
	    }
    }
}