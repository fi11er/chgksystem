﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Infrastructure.Abstract;
using ChgkSystem.WebUI.Models;

namespace ChgkSystem.WebUI.Controllers
{
    public class NewsController : Controller
    {
		public int PageSize = 4;
		private IUnitOfWork unitOfWork;
		public NewsController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

        public ViewResult List(int page = 1)
        {
			IEnumerable<News> news = unitOfWork.NewsRepository.GetAll();
            return View(new Paginator<News> { Collection = news, CurrentPageNumber = page, ItemsPerPage = PageSize });
        }

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult Add(News news)
		{
			if (ModelState.IsValid)
			{
				unitOfWork.NewsRepository.Add(news);
				return Json(new { status = "OK" });
			} 
			else
			{
				return PartialView("_AddForm", news);
			}
		}

		public PartialViewResult Widget()
		{
			IEnumerable<News> news = this.unitOfWork.NewsRepository.GetAll().Take(4);
			return PartialView(news);
		}
    }
}