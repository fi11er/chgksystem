﻿using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class GroupController : Controller
    {
		private IUnitOfWork unitOfWork;
		public GroupController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult Add(Group group)
		{
			if (ModelState.IsValid)
			{
				Tournament t = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == group.TournamentID);
				if (t.UserID != HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}
				t.Groups.Add(group);
				unitOfWork.TournamentRepository.Edit(t);
				string url = Url.Action("details", "tournament", new { ID = t.ID }) + "#groups";
				return Json(new { status = "OK", url = url });
			}
			else
			{
				return PartialView("Group/_AddForm", group);
			}
		}
    }
}