﻿using System.Collections.Generic;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.Parsers;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class QuestionsController : Controller
    {
		private IUnitOfWork unitOfWork;
	    private IQuestionsFileParser parser;
		public QuestionsController(IUnitOfWork unitOfWork, IQuestionsFileParser parser)
		{
			this.unitOfWork = unitOfWork;
			this.parser = parser;
		}

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult AddFile(QuestionsFileModel form)
		{
			if (ModelState.IsValid)
			{
				List<Question> questions = parser.Parse(form.File.InputStream);
				Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
				if (tour.Tournament.UserID != HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}
				tour.Questions = questions;
				unitOfWork.TourRepository.Edit(tour);
				string url = Url.Action("details", "tournament", new { ID = tour.TournamentID }) + "#tours";
				return Json(new { status = "OK", url = url });
			}
			else
			{
				return Json(new { status = "Error" });
			}
		}

	    [HttpGet]
	    [Authorize(Roles = "Managers, Administrators")]
	    public ActionResult EditVisibility(int tourID)
	    {
			Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == tourID);
		    QuestionsVisibilityFormModel model = new QuestionsVisibilityFormModel
		    {
			    QuestionsVisibility = tour.QuestionsVisibility,
			    TourID = tourID
		    };
			return PartialView("Questions/_VisibilityForm", model);
	    }

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
	    public ActionResult EditVisibility(QuestionsVisibilityFormModel form)
	    {
			if (ModelState.IsValid)
			{
				Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
				tour.QuestionsVisibility = form.QuestionsVisibility;
				unitOfWork.TourRepository.Edit(tour);
				string url = Url.Action("details", "tournament", new { ID = tour.TournamentID }) + "#tours";
				return Json(new { status = "OK", url = url });
			}
			else
			{
				return PartialView("Questions/_VisibilityForm");
			}
	    }
    }
}