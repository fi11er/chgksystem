﻿using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Concrete;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Infrastructure.Abstract;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace ChgkSystem.WebUI.Controllers
{
    public class AccountController : Controller
    {
		private IIdentityUnitOfWork authProvider;

		public AccountController(IIdentityUnitOfWork authProvider)
		{
			this.authProvider = authProvider;
		}

		public PartialViewResult LoginMenu()
		{
			User user = null;
			if (HttpContext.User.Identity.IsAuthenticated) {
				user = HttpContext.GetOwinContext().GetUserManager<EFUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result;
			}
			//authProvider.UserManager.cr
			return PartialView("Partial/_LoginMenu", user);
		}

		public ActionResult Login(LoginFormModel form)
		{
			if (ModelState.IsValid)
			{
				User user = authProvider.UserManager.Find(form.Email, form.Password);
				if (user ==null)
				{
					ModelState.AddModelError("", "Неверное имя пользователя или пароль");
				}
				else
				{
					ClaimsIdentity ident = authProvider.UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
					authProvider.AuthManager.SignOut();
					authProvider.AuthManager.SignIn(new AuthenticationProperties
						{
							IsPersistent = false
						},
						ident);
				}
				return Content("");
			}
			else
			{
				return PartialView("Forms/_LoginForm", form);
			}
		}

		public ActionResult Register(RegisterFormModel form)
		{
			if (ModelState.IsValid)
			{
				User user = new User
				{
					UserName = form.Email,
					Email = form.Email
				};
				IdentityResult result = authProvider.UserManager.Create(user, form.Password);
				if (result.Succeeded)
				{
					return Content("");
				}
			}
			return View("Forms/_RegisterForm", form);
		}

		public ActionResult Logout()
		{
			authProvider.AuthManager.SignOut();
			return Content("");
		}

	    public ActionResult Users(string query)
	    {
		    var users = authProvider.UserManager.Users.Where(m => m.Email.Contains(query)).Take(5).Select(m => new { id = m.Id, name = m.Email }).ToList();
		    return Json(users);
	    }
    }
}