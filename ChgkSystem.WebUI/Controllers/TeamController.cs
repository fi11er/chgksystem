﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class TeamController : Controller
    {
		private IUnitOfWork unitOfWork;
		public TeamController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpPost]
		[Authorize]
		public ActionResult Add(AddTeamFormModel form)
		{
			if (ModelState.IsValid)
			{
				if (unitOfWork.PlaygroundRepository.FindOneBy(m => m.ID == form.PlaygroundID).UserID !=
				    HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}

				var team = new Team
				{
					Name = form.Name,
					PlaygroundID = form.PlaygroundID,
					Status = TeamStatus.Accepted
				};
				team.Players = new List<Player>();
				foreach (var player in form.Players.Split('\n'))
				{
					if (player.Length == 0)
					{
						continue;
					}
					team.Players.Add(new Player { Name = player.Trim() });
				}
				team.Groups = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == form.TournamentID)
					.Groups.Where(g => form.GroupIDs.Contains(g.ID)).ToList();
				unitOfWork.TeamRepository.Add(team);
				return Json(new { status = "OK" });
			}
			else
			{
				form.Tournament = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == form.Tournament.ID);
				return PartialView("Team/_AddForm", form);
			}
		}

		[HttpPost]
		public ActionResult Register(RegisterTeamFormModel form)
		{
			if (ModelState.IsValid)
			{
				var team = new Team
				{
					Name = form.Name,
					PlaygroundID = form.PlaygroundID,
					Status = TeamStatus.Pending
				};
				if (form.Players != null)
				{
					team.Players = new List<Player>();
					foreach (var player in form.Players.Split('\n'))
					{
						if (player.Length == 0)
						{
							continue;
						}
						team.Players.Add(new Player { Name = player.Trim() });
					}	
				}
				unitOfWork.TeamRepository.Add(team);
				return Json(new { status = "OK" });
			}
			else
			{
				form.Tournament = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == form.Tournament.ID);
				return PartialView("Team/_RegisterForm", form);
			}
		}

		public ActionResult Submit(TeamSubmitFormModel form)
		{
			if (ModelState.IsValid)
			{
				Team team = unitOfWork.TeamRepository.FindOneBy(m => m.ID == form.TeamID);
				if (team.Playground.UserID != HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}

				team.Status = form.Status;
				unitOfWork.TeamRepository.Edit(team);
				return Json(new { status = "OK" });
			}
			else
			{
				return Json(new { status = "Error" });
			}
		}
    }
}