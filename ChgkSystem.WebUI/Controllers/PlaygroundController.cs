﻿using System.Collections.Generic;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class PlaygroundController : Controller
	{
		private IUnitOfWork unitOfWork;
		public PlaygroundController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpPost]
		[Authorize]
		public ActionResult Add(PlaygroundFormModel form)
		{
			if (ModelState.IsValid)
			{
				var playground = new Playground
				{
					Title = form.Title,
					TournamentID = form.TournamentID,
					UserID = HttpContext.User.Identity.GetUserId()
				};
				playground.Locations = new List<Location>();
				foreach (var location in form.Locations.Split('\n'))
				{
					if (location.Length == 0)
					{
						continue;
					}
					playground.Locations.Add(new Location { Title = location.Trim() });
				}
				unitOfWork.PlaygroundRepository.Add(playground);
				return Json(new { status = "OK" });
			}
			else
			{
				return PartialView("Playground/_AddForm");
			}
		}

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult Submit(PlaygroundSubmitFormModel form)
		{
			if (ModelState.IsValid)
			{
				Playground playground = unitOfWork.PlaygroundRepository.FindOneBy(m => m.ID == form.PlaygroundID);
				if (playground.Tournament.UserID != HttpContext.User.Identity.GetUserId())
				{
					return Json(new { status = "Error" });
				}

				playground.Status = form.Status;
				unitOfWork.PlaygroundRepository.Edit(playground);
				return Json(new { status = "OK" });
			}
			else
			{
				return Json(new { status = "Error" });
			}
		}

		public PartialViewResult Teams(int playgroundID)
		{
			Playground playground = unitOfWork.PlaygroundRepository.FindOneBy(m => m.ID == playgroundID);
			return PartialView("Playground/_Teams", playground);
		}
    }
}