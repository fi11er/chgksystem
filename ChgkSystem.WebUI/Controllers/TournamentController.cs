﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Concrete;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Infrastructure.Abstract;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ChgkSystem.WebUI.Controllers
{
    public class TournamentController : Controller
    {
		private IIdentityUnitOfWork unitOfWork;
		//private IAuthProvider authProvider;
		public TournamentController(IIdentityUnitOfWork unitOfWork)//, IAuthProvider authProvider)
		{
			this.unitOfWork = unitOfWork;
			//this.authProvider = authProvider;
		}

        public ViewResult All()
        {
			IEnumerable<Tournament> tournaments = unitOfWork.TournamentRepository.GetAll();

			ViewBag.TournamentPage = "All";
			return View("List", tournaments);
        }

		public ViewResult Past()
		{
			IEnumerable<Tournament> tournaments = unitOfWork.TournamentRepository.GetPast();

			ViewBag.TournamentPage = "Past";
			return View("List", tournaments);
		}

		public ViewResult Active()
		{
			IEnumerable<Tournament> tournaments = unitOfWork.TournamentRepository.GetActive();

			ViewBag.TournamentPage = "Active";
			return View("List", tournaments);
		}

		public ViewResult Next()
		{
			IEnumerable<Tournament> tournaments = unitOfWork.TournamentRepository.GetNext();

			ViewBag.TournamentPage = "Next";
			return View("List", tournaments);
		}

		public ViewResult Details(int id)
		{
			Tournament tournament = unitOfWork.TournamentRepository.FindOneBy(m => m.ID == id);
			return View(tournament);
		}

		[HttpPost]
		[Authorize(Roles = "Managers, Administrators")]
		public ActionResult Add(AddTournamentFormModel form)
		{
			if (ModelState.IsValid)
			{
				form.Tournament.UserID = HttpContext.User.Identity.GetUserId(); //authProvider.Identity.GetUserId();
				form.Tournament.Jury = new List<User>();
				foreach (var jury in form.Jury)
				{
					var user = unitOfWork.UserManager.Users.FirstOrDefault(m => m.Id == jury);
					if (user == null)
					{
						return PartialView("_AddForm", form);
					}
					form.Tournament.Jury.Add(user);
				}
				unitOfWork.TournamentRepository.Add(form.Tournament);
				return Json(new { status = "OK" });
			}
			else
			{
				return PartialView("_AddForm", form);
			}
		}
    }
}