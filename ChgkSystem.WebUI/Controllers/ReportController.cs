﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Entities;
using ChgkSystem.Parsers;
using ChgkSystem.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace ChgkSystem.WebUI.Controllers
{
    public class ReportController : Controller
    {
		private IUnitOfWork unitOfWork;
	    private IReportFileParser parser;

	    public ReportController(IUnitOfWork unitOfWork, IReportFileParser parser)
		{
			this.unitOfWork = unitOfWork;
			this.parser = parser;
		}

		[HttpPost]
		[Authorize]
	    public ActionResult AddFile(ReportFileModel form)
	    {
			if (ModelState.IsValid)
			{
				var userId = HttpContext.User.Identity.GetUserId();
				Tour tour = unitOfWork.TourRepository.FindOneBy(m => m.ID == form.TourID);
				Playground playground = unitOfWork.PlaygroundRepository.FindOneBy
					(
						m => m.UserID == userId
							&& m.TournamentID == tour.TournamentID
					);
				if (playground == null)
				{
					return Json(new { status = "Error" });
				}

				if (!parser.Parse(form.File.InputStream))
				{
					return Json(new { status = "Error" });
				}

				foreach (var reportItem in parser.Result)
				{
					var question = tour.Questions.FirstOrDefault(m => m.Number == reportItem.Question);
					if (question == null)
					{
						return Json(new { status = "Error" });
					}
					var answer = question.Answers.FirstOrDefault(m => m.Text == reportItem.Answer);
					if (answer == null)
					{
						unitOfWork.AnswerRepository.Add(new Answer
						{
							QuestionID = question.ID,
							Text = reportItem.Answer,
							Status = AnswerStatus.Pending
						});
						answer = question.Answers.First(m => m.Text == reportItem.Answer);
					}
					var team = playground.Teams.FirstOrDefault(m => m.Code == reportItem.TeamCode);
					if (team == null)
					{
						return Json(new { status = "Error" });
					}
					if (answer.TeamAnswers == null)
					{
						answer.TeamAnswers = new List<TeamAnswer>();
					}
					if (answer.TeamAnswers.Any(m => m.Team == team))
					{
						return Json(new { status = "Error" });
					}
					answer.TeamAnswers.Add(new TeamAnswer {Team = team});
					unitOfWork.Save();
				}

				tour.Reports.Add(new Report { PlaygroundID = playground.ID, Date = DateTime.Now });
				if (tour.Reports.Count == tour.Tournament.AcceptedPlaygrounds.Count())
				{
					tour.Status = TourStatus.CheckingProtocols;
				}
				unitOfWork.Save();
				
				
				string url = Url.Action("details", "tournament", new { ID = tour.TournamentID }) + "#tours";
				return Json(new { status = "OK", url = url });
			}
			else
			{
				return Json(new { status = "Error" });
			} 
	    }
    }
}