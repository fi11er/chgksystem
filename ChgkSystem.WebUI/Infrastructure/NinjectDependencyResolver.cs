﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Concrete;
using ChgkSystem.Parsers;
using ChgkSystem.WebUI.Infrastructure.Abstract;
using ChgkSystem.WebUI.Infrastructure.Concrete;
using Ninject;

namespace ChgkSystem.WebUI.Infrastructure
{
	public class NinjectDependencyResolver : IDependencyResolver
	{
		private IKernel kernel;

		public NinjectDependencyResolver(IKernel kernelParam)
		{
			kernel = kernelParam;
			AddBindings();
		}

		public object GetService(Type serviceType)
		{
			return kernel.TryGet(serviceType);
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return kernel.GetAll(serviceType);
		}

		private void AddBindings()
		{
			kernel.Bind<IUnitOfWork>().To<EFUnitOfWork>();
			kernel.Bind<IIdentityUnitOfWork>().To<IdentityUnitOfWork>();
			kernel.Bind<IQuestionsFileParser>().To<QuestionsFileParser>();
			kernel.Bind<IReportFileParser>().To<ReportFileParser>();
		}
	}
}