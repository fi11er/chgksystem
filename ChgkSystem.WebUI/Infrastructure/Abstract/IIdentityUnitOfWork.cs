﻿using System.Security.Principal;
using ChgkSystem.Domain.Abstract;
using ChgkSystem.Domain.Concrete;
using Microsoft.Owin.Security;

namespace ChgkSystem.WebUI.Infrastructure.Abstract
{
	public interface IIdentityUnitOfWork: IUnitOfWork
	{
		EFUserManager UserManager { get; }
		IAuthenticationManager AuthManager { get; }
		IIdentity Identity { get; }

	}
}
