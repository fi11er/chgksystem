﻿using System.Security.Principal;
using System.Web;
using ChgkSystem.Domain.Concrete;
using ChgkSystem.Domain.Entities;
using ChgkSystem.WebUI.Infrastructure.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace ChgkSystem.WebUI.Infrastructure.Concrete
{
	public class IdentityUnitOfWork : EFUnitOfWork, IIdentityUnitOfWork
	{
		private EFUserManager userManager;

		public EFUserManager UserManager
		{
			get
			{

				if (this.userManager == null)
				{
					this.userManager = new EFUserManager(new UserStore<User>(context));
				}
				return userManager;
			}
		}

		public IAuthenticationManager AuthManager
		{
			get
			{
				return HttpContext.Current.GetOwinContext().Authentication;
				
			}
		}

		public IIdentity Identity
		{
			get
			{
				return HttpContext.Current.User.Identity;
			}
		}
	}
}