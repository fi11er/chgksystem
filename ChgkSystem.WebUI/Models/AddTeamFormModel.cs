﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class AddTeamFormModel
	{
		public AddTeamFormModel()
		{
			GroupIDs = new int[0];
		}

		[DisplayName("Название команды *")]
		[Required(ErrorMessage = "Введите название команды")]
		public string Name { get; set; }
		[DisplayName("Состав")]
		public string Players { get; set; }
		[DisplayName("Отчетная площадка *")]
		[Required(ErrorMessage = "Выберите отчетную площадку")]
		public int PlaygroundID { get; set; }
		[DisplayName("Участвует в группах *")]
		[Required(ErrorMessage = "Укажите хотя бы одну группу")]
		public int[] GroupIDs { get; set; }
		[Required(ErrorMessage = "Укажите турнир")]
		public int TournamentID { get; set; }

		public virtual Tournament Tournament { get; set; }
	}
}