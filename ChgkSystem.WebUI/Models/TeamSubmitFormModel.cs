﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class TeamSubmitFormModel
	{
		public int TeamID { get; set; }
		public TeamStatus Status { get; set; }
	}
}