﻿using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class PlaygroundSubmitFormModel
	{
		public int PlaygroundID { get; set; }
		public PlaygroundStatus Status { get; set; }
	}
}