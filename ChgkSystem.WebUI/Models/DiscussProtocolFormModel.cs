﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class DiscussProtocolFormModel
	{
		public int TourID { get; set; }
		public Dictionary<string, JuryDecision> Answers { get; set; }

		public IEnumerable<Question> Questions { get; set; } 
	}
}