﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class AddTournamentFormModel
	{
		public Tournament Tournament { get; set; }
		public List<string> Jury { get; set; }
	}
}