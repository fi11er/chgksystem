﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class AddAppealFormModel
	{
		public Appeal Appeal { get; set; }
		public int TourID { get; set; }
		public virtual Tour Tour { get; set; }
	}
}