﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{

	public class TournamentDetailsViewModel
	{
		public Tournament Tournament { get; set; }
		public bool IsLogged { get; set; }
		public bool IsManager { get; set; }
	}
}