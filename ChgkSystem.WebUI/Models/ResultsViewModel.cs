﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class TeamViewModel
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string Playground { get; set; }
		public IEnumerable<int> Tours { get; set; }
 		public int Total { get; set; }
	}

	public class ResultsViewModel
	{
		public int TourID { get; set; }
		public int GroupID { get; set; }
		public int PlaygroundID { get; set; }
		public Tournament Tournament { get; set; }
		public IEnumerable<TeamViewModel> Teams { get; set; } 
	}
}