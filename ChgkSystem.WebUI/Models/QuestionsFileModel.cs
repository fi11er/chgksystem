﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ChgkSystem.WebUI.Models
{
	public class QuestionsFileModel
	{
		public int TourID { get; set; }
		[Required(ErrorMessage = "Выберите файл с вопросами")]
		//[FileExtensions(Extensions = {".txt"}, ErrorMessage = "Файл должен быть в формате txt")]
		public HttpPostedFileBase File { get; set; }
	}
}