﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ChgkSystem.WebUI.Models
{
	public class ReportFileModel
	{
		public int TourID { get; set; }
		[DisplayName("Файл с отчетом")]
		[Required(ErrorMessage = "Выберите файл с отчетом")]
		//[FileExtensions(Extensions = {".txt"}, ErrorMessage = "Файл должен быть в формате txt")]
		public HttpPostedFileBase File { get; set; }
	}
}