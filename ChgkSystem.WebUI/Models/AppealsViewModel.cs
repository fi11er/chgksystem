﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class AppealsViewModel
	{
		public int TourID { get; set; }

		public IEnumerable<Appeal> Appeals { get; set; }
		public string AppealsText { get; set; }
	}
}