﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	[Bind(Exclude = "Appeals")]
	public class AppealReviewFormModel
	{
		public int TourID { get; set; }
		public Dictionary<string, AppealStatus> Reviews { get; set; }
 
		public IEnumerable<Appeal> Appeals { get; set; } 
	}
}