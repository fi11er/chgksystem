﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ChgkSystem.WebUI.Models
{
	public class RegisterTeamFormModel: AddTeamFormModel
	{
		[DisplayName("Электронная почта *")]
		[Required(ErrorMessage = "Введите ваш адрес электронной почты")]
		public string Email { get; set; }
	}
}