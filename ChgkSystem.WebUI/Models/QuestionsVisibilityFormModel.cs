﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.WebUI.Models
{
	public class QuestionsVisibilityFormModel
	{
		public int TourID { get; set; }
		public QuestionsVisibility QuestionsVisibility { get; set; }
	}
}