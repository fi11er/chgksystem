﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ChgkSystem.WebUI.Models
{
	public class PlaygroundFormModel
	{
		public int TournamentID { get; set; }
		[Required(ErrorMessage = "Введите название площадки")]
		[DisplayName("Название площадки *")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Введите список игровых площадок")]
		[DisplayName("Список игровых площадок *")]
		public string Locations { get; set; }
	}
}