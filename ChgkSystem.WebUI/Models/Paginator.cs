﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChgkSystem.WebUI.Models
{
	public class Paginator<T>
	{
		public IEnumerable<T> Collection { get; set; }
		public int ItemsPerPage { get; set; }
		public int CurrentPageNumber { get; set; }
		public int TotalItems
		{
			get
			{
				return Collection.Count();
			}
		}
		public int TotalPages
		{
			get
			{
				return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
			}
		}
		public IEnumerable<T> CurrentPage
		{
			get 
			{
				return Collection.Skip((CurrentPageNumber - 1) * ItemsPerPage).Take(ItemsPerPage);
			}
		}
	}
}