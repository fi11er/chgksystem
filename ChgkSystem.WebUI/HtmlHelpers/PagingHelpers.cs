﻿using System;
using System.Text;
using System.Web.Mvc;
using ChgkSystem.WebUI.Models;

namespace ChgkSystem.WebUI.HtmlHelpers
{
	public static class PagingHelpers
	{
		public static MvcHtmlString PageLinks<T>(this HtmlHelper html, Paginator<T> paginator, Func<int, string> pageUrl)
		{
			StringBuilder result = new StringBuilder();
			for (int i = 1; i <= paginator.TotalPages; i++)
			{
				TagBuilder a = new TagBuilder("a");
				a.MergeAttribute("href", pageUrl(i));
				a.InnerHtml = i.ToString();

				TagBuilder li = new TagBuilder("li");
				li.InnerHtml = a.ToString();
				if (i == paginator.CurrentPageNumber)
				{
					li.AddCssClass("active");
				}
				result.Append(li.ToString());
			}
			TagBuilder ul = new TagBuilder("ul");
			ul.AddCssClass("pagination");
			ul.InnerHtml = result.ToString();
			return MvcHtmlString.Create(ul.ToString());
		}
	}
}