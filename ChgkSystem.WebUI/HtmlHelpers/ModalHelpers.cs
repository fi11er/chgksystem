﻿using System;
using System.Web.Mvc;

namespace ChgkSystem.WebUI.HtmlHelpers
{
	public class BootstrapModal: IDisposable
	{
		protected HtmlHelper helper;

		public BootstrapModal(HtmlHelper helper, string id, string title, string classes = "")
		{
			this.helper = helper;
			helper.ViewContext.Writer.Write(
				"<div id=\"" + id + "\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"" + id + "Label\" aria-hidden=\"true\">"
				+ "<div class=\"modal-dialog " + classes + "\">"
						+ "<div class=\"modal-content no-radius\">"
							+ "<div class=\"modal-header\">"
								+ "<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>"
								+ "<h4 class=\"modal-title\" id=\"" + id + "Label\">" + title + "</h4>"
							+ "</div>");
		}

		public BootstrapModal(HtmlHelper helper, string id, int dumb = 0, string classes = "")
		{
			this.helper = helper;
			helper.ViewContext.Writer.Write(
				"<div id=\"" + id + "\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"" + id + "Label\" aria-hidden=\"true\">"
				+ "<div class=\"modal-dialog " + classes + "\">"
						+ "<div class=\"modal-content no-radius\">"
							+ "<button type=\"button\" class=\"close padded\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>"
							+ "<br />");
		}

		public void Dispose()
		{
			helper.ViewContext.Writer.Write("</div></div></div>");
		}
	}

	public static class ModalHelpers
	{
		public static BootstrapModal BeginModal(this HtmlHelper self, string id, string title, string classes = "")
		{
			return new BootstrapModal(self, id, title, classes);
		}

		public static BootstrapModal BeginModal(this HtmlHelper self, string id, int dumb = 0, string classes = "")
		{
			return new BootstrapModal(self, id, classes: classes);
		}
	}
}