﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Parsers
{
	public class QuestionsFileParser: IQuestionsFileParser
	{
		private Stream stream;

		public List<Question> Parse(Stream s)
		{
			this.stream = s;

			StreamReader reader = new StreamReader(stream);
			List<Question> questions = new List<Question>();

			string nextString = reader.ReadLine();
			if (nextString == null || nextString[0] != '?')
			{
				// ERROR
			}
			int questionNumber = 1;
			Question currentQuestion = new Question() { Answers = new List<Answer>(), Number = questionNumber };
			Answer currentAnswer = null;
			StringBuilder currentText = new StringBuilder(nextString.Substring(1) + '\n');

			while ((nextString = reader.ReadLine()) != null)
			{
				if (nextString.Length == 0)
				{
					continue;
				}
				switch (nextString[0])
				{
					case '?':
						if (!currentQuestion.CorrectAnswers.Any())
						{
							// ERROR
						}
						currentAnswer.Text = currentText.ToString().Trim();
						questions.Add(currentQuestion);

						currentQuestion = new Question() { Answers = new List<Answer>(), Number = ++questionNumber };
						currentAnswer = null;
						currentText = new StringBuilder(nextString.Substring(1) + '\n');
						break;
					case '+':
					case '-':
						if (currentAnswer == null)
						{
							currentQuestion.Text = currentText.ToString().Trim();
						}
						else
						{
							currentAnswer.Text = currentText.ToString().Trim();
						}

						var status = nextString[0] == '+' ? AnswerStatus.Correct : AnswerStatus.Incorrect;

						currentAnswer = new Answer() { Status = status };
						currentQuestion.Answers.Add(currentAnswer);
						currentText = new StringBuilder(nextString.Substring(1) + '\n');
						break;
					case '\\':
						currentText.Append(nextString.Substring(1) + '\n');
						break;
					case '#':
						continue;
					default:
						currentText.Append(nextString + '\n');
						break;
				}
			}
			if (!currentQuestion.CorrectAnswers.Any())
			{
				// ERROR
			}
			currentAnswer.Text = currentText.ToString().Trim();
			questions.Add(currentQuestion);
			return questions;
		}
	}
}
