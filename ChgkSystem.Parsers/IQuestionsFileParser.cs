﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Parsers
{
	public interface IQuestionsFileParser
	{
		List<Question> Parse(Stream stream);
	}
}
