﻿using System;
using System.Collections.Generic;
using System.IO;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Parsers
{
	public class ReportFileParser: IReportFileParser
	{
		private Stream stream;

		public List<string> Errors { get; private set; }
		public List<ReportItem> Result { get; private set; }
		public bool Parse(Stream s)
		{
			stream = s;
			StreamReader reader = new StreamReader(stream);
			List<ReportItem> result = new List<ReportItem>();
			string nextString;
			int currentQuestion = 0;
			while ((nextString = reader.ReadLine()) != null)
			{
				if (nextString.Length == 0)
				{
					continue;
				}
				if (nextString[0] == '?')
				{
					if (!int.TryParse(nextString.Substring(1), out currentQuestion))
					{
						return false;
					}
				} 
				else if (nextString[0] == '#')
				{
					continue;
				}
				else
				{
					var strings = nextString.Split(new[] {' ', '\t'}, 2);
					if (strings.Length != 2)
					{
						return false;
					}
					var teams = strings[0].Split(new[] {','});
					var answer = strings[1];
					int n;
					foreach (var team in teams)
					{
						if (team[0] != 'T' || !int.TryParse(team.Substring(1), out n))
						{
							return false;
						}
						result.Add(new ReportItem
						{
							Answer = answer,
							Question = currentQuestion,
							TeamCode = team
						});
					}
				}

			}
			Result = result;
			return true;
			//Result = new List<ReportItem>
			//{
			//	new ReportItem
			//	{
			//		Answer = "Верный овтет",
			//		Question = 1,
			//		TeamCode = "T1"
			//	},
			//	new ReportItem
			//	{
			//		Answer = "Верный ответ",
			//		Question = 1,
			//		TeamCode = "T2"
			//	},
			//	new ReportItem
			//	{
			//		Answer = "Верный овтет",
			//		Question = 2,
			//		TeamCode = "T2"
			//	},
			//	new ReportItem
			//	{
			//		Answer = "фывыа",
			//		Question = 2,
			//		TeamCode = "T1"
			//	}
			//};
		}
	}
}
