﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChgkSystem.Domain.Entities;

namespace ChgkSystem.Parsers
{
	public class ReportItem
	{
		public int Question { get; set; }
		public string TeamCode { get; set; }
		public string Answer { get; set; }
		public bool IsCorrect { get; set; }

		public int TeamID
		{
			get { return Convert.ToInt32(TeamCode.Substring(1)); }
		}
	}

	public interface IReportFileParser
	{
		List<string> Errors { get; }
		List<ReportItem> Result { get; } 
		bool Parse(Stream stream);
	}
}
